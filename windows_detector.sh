#!/usr/bin/env bash 
# This function is under license GPLv3 http://www.gnu.org/licenses/gpl.txt  

windows_detector()
{
	os_search=`os-prober`
	os_check=`echo "$os_search" > os_installed`
	os_read=`while read line
                 do
                 echo -e "$line\n"
                 done < os_installed`
         
        for w in $os_read
        do
        if [ "$w" == '7' ]
        then
        os_version='Windows 7'
        export os_version
        fi

        if [ "$w" == '8' ]
        then
        os_version='Windows 8'
        export os_version
        fi
         
        if [ "$w" == '(loader):Windows:chain' ]
        then
        echo -e "\n${BOLD}$os_version semble être installé sur votre machine.${RESETC}"
        fi
        done
        echo -e "${YELLOW}Pour plus de détails , tapez [info]${RESETC}"
        read info_read
        if [ "$info_read" == "info" ]
        then
        echo "$os_read"
        fi 
}
exit
